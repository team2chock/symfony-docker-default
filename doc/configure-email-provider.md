2 Configure Email provider !
============================

You are here => [Summary](/README.md) > 1 Get Started !

***

Requirements  
------------

  * Docker on your host machine;
  * Docker Compose on your host machine;

## Usage  

1. Example of Gmail configuration
2. Example of Ionos configuration ( **read it if your email provider is your web hosting** )
3. Popular SMTP configurations for outgoing emails 

## 1 . Example of gmail configuration ( in 4 steps )

In this example, we will pretend that your email address is **my-email-address@gmail.com**

* 1/4 - Activate the 2 factor authentication on your Gmail account by following the instructions there => https://support.google.com/accounts/answer/185839

* 2/4 - Create an application password by following the instructions there => https://support.google.com/mail/answer/185833. We will pretend that your generated application password is **1234567890123456**

* 3/4 - Provide the SMTP information in your docker-compose template, **be careful read the detail below :**  

  Gmail is interesting because it exposes 2 different ports for SMTP :  

  - 465 : This port support SSL -> if you use this port, you must use a docker-compose template with name **docker-compose__\*\*maildev-outgoing-secure.yml** ( maildev is set in secure mode )  

  - 587 : This port does not support SSL instead it support TLS -> if you use this port, you must use a docker-compose template with name **docker-compose__\*\*maildev.yml** ( maildev is set in normal mode )  
  
  Let's see examples with normal mode and secure mode

   A - Normal mode :



```yml
# /templates/docker-compose__php__maildev.yml

######################################################################################################################################################
##  ** VARIABLES TO REPLACE **
##
##  - DOCKER_CONTAINER_ALL_TIMEZONE = timezone to use in your docker containers ( https://en.wikipedia.org/wiki/List_of_tz_database_time_zones )
##  - DOCKER_HOST_PHP_PORT = port to allocate for php on host machine
##  - SMTP_HOST = smtp server host for mailing
##  - SMTP_PORT = smtp server port for mailing
##  - SMTP_USER = email address for mailing
##  - SMTP_PASSWORD = email address password for mailing
##  - DOCKER_HOST_MAILDEV_RELAY_WEB_UI_PORT = port to allocate for maildev web interface on host machine
##
######################################################################################################################################################
version: '3'
services:
  php:
    build: ../build/php
    restart: always
    environment:
        - TZ=Europe/Paris
    expose:
        - '9000'
    depends_on:
        - maildev
    volumes:
        - ../symfony:/var/www/html/symfony
        - ../logs:/var/log
    ports:
        - '8001:9000'

  maildev:
    image: djfarrelly/maildev
    restart: always
    environment:
        - TZ=Europe/Paris
    command: bin/maildev
        --web 80
        --smtp 25
        --outgoing-host 'smtp.gmail.com'
        --outgoing-port '587'
        --outgoing-user 'my-email-address@gmail.com'
        --outgoing-pass '1234567890123456'
        --auto-relay
    ports:
        - '8002:80'


```

   A - Secure mode :


```yml
# /templates/docker-compose__php__maildev-outgoing-secure.yml

######################################################################################################################################################
##  ** VARIABLES TO REPLACE **
##
##  - DOCKER_CONTAINER_ALL_TIMEZONE = timezone to use in your docker containers ( https://en.wikipedia.org/wiki/List_of_tz_database_time_zones )
##  - DOCKER_HOST_PHP_PORT = port to allocate for php on host machine
##  - SMTP_HOST = smtp server host for mailing
##  - SMTP_PORT = smtp server port for mailing
##  - SMTP_USER = email address for mailing
##  - SMTP_PASSWORD = email address password for mailing
##  - DOCKER_HOST_MAILDEV_RELAY_WEB_UI_PORT = port to allocate for maildev web interface on host machine
##
######################################################################################################################################################
version: '3'
services:
  php:
    build: ../build/php
    restart: always
    environment:
        - TZ=Europe/Paris
    expose:
        - '9000'
    depends_on:
        - maildev
    volumes:
        - ../symfony:/var/www/html/symfony
        - ../logs:/var/log
    ports:
        - '8001:9000'

  maildev:
    image: djfarrelly/maildev
    restart: always
    environment:
        - TZ=Europe/Paris
    command: bin/maildev
        --web 80
        --smtp 25
        --outgoing-host 'smtp.gmail.com'
        --outgoing-port '465'
        --outgoing-user 'my-email-address@gmail.com'
        --outgoing-pass '1234567890123456'
        --outgoing-secure
        --auto-relay
    ports:
        - '8002:80'


```

* 4/4 - Provide the SMTP information in your symfony's **parameters.yml** file but be careful.  
**Maildev** acts like a mail relay to intercept outgoing emails, so in the parameters.yml file :  
  - **mailer_host** matchs to the name of Maildev container ( **maildev** )
  - **mailer_port** matchs to the maildev container smtp port ( **25** )


```yml
# This file is auto-generated during the composer install
parameters:
    # ... [code before ignored] ...
    mailer_transport: smtp
    mailer_host: maildev
    mailer_user: my-email-address@gmail.com
    mailer_password: 1234567890123456
    mailer_auth_mode: login
    mailer_port: '25'
    mailer_encryption: null
    # ... [code after ignored] ...
```  

That's it ! You Symfony application is now ready to send emails with Gmail =)  


## 2 . Example of Ionos configuration ( in 3 steps )

In this example, we will pretend that your email address is **my-email-address@mydomainreservedonionos.com** and your password is **ABCDEF**

* 1/3 - **( this step is not necessary if you run on a local machine )** Configure a DNS MX record for your server ( for example if your server is under IP **123.456.789**, you have to create a DNS record of type MX to allow this server with IP **123.456.789** to send email using ionos smtp. You can find some instructions there => https://www.ionos.com/community/email-office/email/managing-email-on-cloud-servers/

* 2/3 - Provide the SMTP information in your docker-compose template, **be careful read the detail below :**  

  Ionos is interesting because it exposes 2 different ports for SMTP :  

  - 465 : This port support SSL -> if you use this port, you must use a docker-compose template with name **docker-compose__\*\*maildev-outgoing-secure.yml** ( maildev is set in secure mode )  

  - 587 : This port does not support SSL instead it support TLS -> if you use this port, you must use a docker-compose template with name **docker-compose__\*\*maildev.yml** ( maildev is set in normal mode )  
  
  Let's see examples with normal mode and secure mode

   A - Normal mode :


```yml
# /templates/docker-compose__php__maildev.yml

######################################################################################################################################################
##  ** VARIABLES TO REPLACE **
##
##  - DOCKER_CONTAINER_ALL_TIMEZONE = timezone to use in your docker containers ( https://en.wikipedia.org/wiki/List_of_tz_database_time_zones )
##  - DOCKER_HOST_PHP_PORT = port to allocate for php on host machine
##  - SMTP_HOST = smtp server host for mailing
##  - SMTP_PORT = smtp server port for mailing
##  - SMTP_USER = email address for mailing
##  - SMTP_PASSWORD = email address password for mailing
##  - DOCKER_HOST_MAILDEV_RELAY_WEB_UI_PORT = port to allocate for maildev web interface on host machine
##
######################################################################################################################################################
version: '3'
services:
  php:
    build: ../build/php
    restart: always
    environment:
        - TZ=Europe/Paris
    expose:
        - '9000'
    depends_on:
        - maildev
    volumes:
        - ../symfony:/var/www/html/symfony
        - ../logs:/var/log
    ports:
        - '8001:9000'

  maildev:
    image: djfarrelly/maildev
    restart: always
    environment:
        - TZ=Europe/Paris
    command: bin/maildev
        --web 80
        --smtp 25
        --outgoing-host 'smtp.ionos.fr'
        --outgoing-port '587'
        --outgoing-user 'my-email-address@mydomainreservedonionos.com'
        --outgoing-pass 'ABCDEF'
        --auto-relay
    ports:
        - '8002:80'


```

   A - Secure mode :


```yml
# /templates/docker-compose__php__maildev-outgoing-secure.yml

######################################################################################################################################################
##  ** VARIABLES TO REPLACE **
##
##  - DOCKER_CONTAINER_ALL_TIMEZONE = timezone to use in your docker containers ( https://en.wikipedia.org/wiki/List_of_tz_database_time_zones )
##  - DOCKER_HOST_PHP_PORT = port to allocate for php on host machine
##  - SMTP_HOST = smtp server host for mailing
##  - SMTP_PORT = smtp server port for mailing
##  - SMTP_USER = email address for mailing
##  - SMTP_PASSWORD = email address password for mailing
##  - DOCKER_HOST_MAILDEV_RELAY_WEB_UI_PORT = port to allocate for maildev web interface on host machine
##
######################################################################################################################################################
version: '3'
services:
  php:
    build: ../build/php
    restart: always
    environment:
        - TZ=Europe/Paris
    expose:
        - '9000'
    depends_on:
        - maildev
    volumes:
        - ../symfony:/var/www/html/symfony
        - ../logs:/var/log
    ports:
        - '8001:9000'

  maildev:
    image: djfarrelly/maildev
    restart: always
    environment:
        - TZ=Europe/Paris
    command: bin/maildev
        --web 80
        --smtp 25
        --outgoing-host 'smtp.ionos.fr'
        --outgoing-port '465'
        --outgoing-user 'my-email-address@mydomainreservedonionos.com'
        --outgoing-pass 'ABCDEF'
        --outgoing-secure
        --auto-relay
    ports:
        - '8002:80'


```

* 3/3 - Provide the SMTP information in your symfony's **parameters.yml** file but be careful.  
**Maildev** acts like a mail relay to intercept outgoing emails, so in the parameters.yml file :  
  - **mailer_host** matchs to the name of Maildev container ( **maildev** )
  - **mailer_port** matchs to the maildev container smtp port ( **25** )


```yml
# This file is auto-generated during the composer install
parameters:
    # ... [code before ignored] ...
    mailer_transport: smtp
    mailer_host: maildev
    mailer_user: my-email-address@mydomainreservedonionos.com
    mailer_password: ABCDEF
    mailer_auth_mode: login
    mailer_port: '25'
    mailer_encryption: null
    # ... [code after ignored] ...
```  

That's it ! You Symfony application is now ready to send emails with Ionos =)


### 3. Popular SMTP configurations for outgoing emails 

Here you can find some smtp configurations for some popular email providers.  
This list is not exhaustive, don't hesitate to browse this link for more results :  
[https://www.arclab.com/en/kb/email/list-of-smtp-and-pop3-servers-mailserver-list.html](https://www.arclab.com/en/kb/email/list-of-smtp-and-pop3-servers-mailserver-list.html)
  
```
> Gmail
--------------------------------------------------------------------------
| Server:     | Host            | Port:        |   Secure Mode Mandatory |
--------------------------------------------------------------------------
| SMTP Server |	smtp.gmail.com  | SSL 465      |           Yes           |
--------------------------------------------------------------------------
| SMTP Server |	smtp.gmail.com  | StartTLS 587 |           No            |
--------------------------------------------------------------------------

> Hotmail
--------------------------------------------------------------------
| Server:     | Host           | Port:   |   Secure Mode Mandatory |
--------------------------------------------------------------------
| SMTP Server |	smtp.live.com  | SSL 465 |           Yes           |
--------------------------------------------------------------------

> Outlook
-------------------------------------------------------------------------
| Server:     | Host           | Port:        |   Secure Mode Mandatory |
-------------------------------------------------------------------------
| SMTP Server |	smtp.live.com  | StartTLS 587 |           No            |
-------------------------------------------------------------------------

> Office 365
------------------------------------------------------------------------------
| Server:     | Host                | Port:        |   Secure Mode Mandatory |
------------------------------------------------------------------------------
| SMTP Server |	smtp.office365.com  | StartTLS 587 |           No            |
------------------------------------------------------------------------------

> 1&1 (1and1)
-------------------------------------------------------------------------
| Server:     | Host           | Port:        |   Secure Mode Mandatory |
-------------------------------------------------------------------------
| SMTP Server |	smtp.1and1.com | StartTLS 587 |           No            |
-------------------------------------------------------------------------

> Ionos (former 1and1)
-------------------------------------------------------------------------
| Server:     | Host           | Port:        |   Secure Mode Mandatory |
-------------------------------------------------------------------------
| SMTP Server |	smtp.ionos.fr  | SSL 465      |           Yes           |
-------------------------------------------------------------------------
| SMTP Server |	smtp.ionos.fr  | StartTLS 587 |           No            |
-------------------------------------------------------------------------

> Yahoo
--------------------------------------------------------------------------
| Server:     | Host                 | Port:   |   Secure Mode Mandatory |
--------------------------------------------------------------------------
| SMTP Server |	smtp.mail.yahoo.com  | SSL 465 |           Yes           |
--------------------------------------------------------------------------
``` 

Troubleshooting
----------------------------

The current examples uses docker-compose 3 version.  

```
