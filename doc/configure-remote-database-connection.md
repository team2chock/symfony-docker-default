4 Configure Remote database connection
========================================

You are here => [Summary](/README.md) > 4 Configure Remote database connection

***

Requirements  
------------

  * Docker on your host machine;
  * Docker Compose on your host machine;

## Usage  

1. Configure local database

## 1 . Configure connection to remote database

In this example, we will simply configure the connection to a remote database for our symfony project and for a phm my admin interface.  

We don't need a local database management system, so let's pick a docker-compose template which contains php my admin but not mariadb.  

We will pretend theses credentials for our remote database :  
        
    -> DATABASE NAME : myremotedbname
    -> DATABASE USER : myremoteuser
    -> DATABASE PASSWORD : myremotesuperpassword
    -> DATABASE PORT : 12345
    -> DATABASE HOST : myremote.databaseonline.com


* 1/2 - Provide the remote database host and port for phpmyadmin :


```yml
# /templates/docker-compose__php__phpmyadmin.yml

######################################################################################################################################################
##  ** VARIABLES TO REPLACE **
##
##  - DOCKER_CONTAINER_ALL_TIMEZONE = timezone to use in your docker containers ( https://en.wikipedia.org/wiki/List_of_tz_database_time_zones )
##  - DOCKER_HOST_PHP_PORT = port to allocate for php on host machine
##  - DATABASE_HOST = database host ( for php my admin )
##  - DATABASE_PORT = database port ( for php my admin )
##  - DOCKER_HOST_PHPMYADMIN_PORT = port to allocate for php my admin on host machine
##
######################################################################################################################################################
version: '3'
services:
  php:
    build: ../build/php
    restart: always
    environment:
        - TZ=Europe/Paris
    expose:
        - '9000'
    volumes:
        - ../symfony:/var/www/html/symfony
        - ../logs:/var/log
    ports:
        - '8001:9000'

  phpmyadmin:
    image: phpmyadmin/phpmyadmin
    restart: always
    environment:
        - PMA_PORT=12345
        - PMA_HOST=myremote.databaseonline.com
        - TZ=Europe/Paris
    ports:
        - '8002:80'


```

* 2/2 - Provide the remote database complete information in your symfony's **parameters.yml**.


```yml
# This file is auto-generated during the composer install
parameters:
    # ... [code before ignored] ...
    database_host: myremote.databaseonline.com
    database_port: 12345
    database_name: myremotedbname
    database_user: myremoteuser
    database_password: myremotesuperpassword
    # ... [code after ignored] ...
```  

That's it ! You Symfony application is now connected to a remote database and your php my admin allow you to administrate your remote database =)  

Troubleshooting
----------------------------

The current examples uses docker-compose 3 version.  

```
