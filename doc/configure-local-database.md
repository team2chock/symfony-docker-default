3 Configure Local database
==========================

You are here => [Summary](/README.md) > 3 Configure Local database

***

Requirements  
------------

  * Docker on your host machine;
  * Docker Compose on your host machine;

## Usage  

1. Configure local database

## 1 . Configure local database

In this example, we will simply configure a local database.  

For this purpose we will use a docker-compose template which includes mariadb a popular database management system.  

This database comes with a php my admin interface to manage the database as admin.  

We will set theses credentials for our database :  

    -> DATABASE NAME : mydb
    -> DATABASE PASSWORD : mysuperpassword

Note : The user root is created automatically and just need to provide it's password, this is why we  

did not provide a DATABASE USER above it is equal to root in any case.

* 1/2 - Set the credentials and host machine allocated port for our database and php my admin


```yml
# /templates/docker-compose__php__mariadb__phpmyadmin.yml

######################################################################################################################################################
##  ** VARIABLES TO REPLACE **
##
##  - DOCKER_CONTAINER_ALL_TIMEZONE = timezone to use in your docker containers ( https://en.wikipedia.org/wiki/List_of_tz_database_time_zones )
##  - DOCKER_HOST_PHP_PORT = port to allocate for php on host machine
##  - DATABASE_NAME = database name ( for mariadb )
##  - DATABASE_PASSWORD = database password ( for mariadb )
##  - DOCKER_HOST_DATABASE_PORT = port to allocate for mariadb database on host machine
##  - DOCKER_HOST_PHPMYADMIN_PORT = port to allocate for php my admin on host machine
##
######################################################################################################################################################
version: '3'
services:
  php:
    build: ../build/php
    restart: always
    environment:
        - TZ=Europe/Paris
    expose:
        - '9000'
    depends_on:
        - mariadb
    volumes:
        - ../symfony:/var/www/html/symfony
        - ../logs:/var/log
    ports:
        - '8001:9000'

  mariadb:
    image: mariadb:10.4.7
    restart: always
    environment:
        - MYSQL_DATABASE=mydb
        - MYSQL_ROOT_PASSWORD=mysuperpassword
        - MYSQL_ROOT_HOST=%
        - TZ=Europe/Paris
    ports:
        - '8002:3306'
    volumes:
        - ../mysql:/var/lib/mysql/data

  phpmyadmin:
    image: phpmyadmin/phpmyadmin
    restart: always
    environment:
        - TZ=Europe/Paris
    links:
        - mariadb
    ports:
        - '8003:80'

```

* 2/2 - Provide the local database information in your symfony's **parameters.yml** file but be careful.  
  - **database_host** matchs to the name of local database container ( **mariadb** )
  - **database_port** matchs to the port of local database container ( **3306** )


```yml
# This file is auto-generated during the composer install
parameters:
    # ... [code before ignored] ...
    database_host: mariadb
    database_port: 3306
    database_name: mydb
    database_user: root
    database_password: mysuperpassword
    # ... [code after ignored] ...
```  

That's it ! You Symfony application now has it own local database ready to use =)  

Troubleshooting
----------------------------

The current examples uses docker-compose 3 version.  

```
