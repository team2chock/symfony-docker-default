1 Get Started !
=============

You are here => [Summary](/README.md) > 1 Get Started !

***

Requirements  
------------

  * Docker on your host machine;
  * Docker Compose on your host machine;

## Usage  

1. Add Symfony project  
2. Choose your docker-compose template to use  
3. Replace the variables in your docker-compose template file  
4. Launch your project !  

## 1 . Add Symfony project


Before starting, your need to copy your symfony project inside of the folder  

symfony of this repository. Then you can start the configuration.

## 2 . Choose your docker-compose template file to use

Open the folder templates, you will find about 20 different configurations.  
pick the one which feet to your needs, for example you can see :  

**docker-compose-default.yml** : a ready to use configuration (perfect for local execution)  
**docker-compose__php.yml** : php  
**docker-compose__php__maildev.yml** : php + maildev  
**docker-compose__php__mariadb__phpmyadmin.yml** : php + local mariadb database + phpmyadmin connected to local mariadb database 
**docker-compose__php__phpmyadmin.yml** : php + phpmyadmin connected to an external database  
etc ..  


## 3 . Replace the variables in your docker-compose template file

After you choose a configuration file, open it and you will see a section called **VARIABLES TO REPLACE**.  
All you need to do is to replace the variables between the **#{** and **}#**.  

For example :  
- search and replace **#{DOCKER_CONTAINER_ALL_TIMEZONE}#** by your timezone,  
- search and replace **#{DOCKER_HOST_PHP_PORT}#** by a free port to allocate for php on host machine  


```yml
# /templates/docker-compose__php.yml

######################################################################################################################################################
##  ** VARIABLES TO REPLACE **
##
##  - DOCKER_CONTAINER_ALL_TIMEZONE = timezone to use in your docker containers ( https://en.wikipedia.org/wiki/List_of_tz_database_time_zones )
##  - DOCKER_HOST_PHP_PORT = port to allocate for php on host machine
##
######################################################################################################################################################
version: '3'
services:
  php:
    build: ../build/php
    restart: always
    environment:
        - TZ=#{DOCKER_CONTAINER_ALL_TIMEZONE}#
    expose:
        - '9000'
    volumes:
        - ../symfony:/var/www/html/symfony
        - ../logs:/var/log
    ports:
        - '#{DOCKER_HOST_PHP_PORT}#:9000'

```  


### 4. Launch your project !  

Everything is almost done, follow theses commands to launch the project.  
We will take as an example, the following configurations :  


```bash
$ cd symfony-docker-default
$ docker-compose -f templates/docker-compose__php.yml build
$ docker-compose -f templates/docker-compose__php.yml up -d
$ docker-compose -f templates/docker-compose__php.yml exec -T php php -d memory_limit=-1 bin/console doctrine:schema:update --force --no-interaction 2>&1
```

- You can see the project here => http://localhost:**#{DOCKER_HOST_PHP_PORT}#**  

Troubleshooting
----------------------------

The current examples uses docker-compose 3 version.  

```
