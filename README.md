Symfony Docker Default
========================

The "Symfony Docker Default" project has been created with the following goals :

- Boost the launch of each backend project made with Symfony + Docker.  
- Start with a base using the recommended best practices of Symfony + Docker.  

This project contains ready to use configurations templates depending on your needs.  
Just pick the one you need then replace variables inside to match to your custom configurations.  

Documentation  
------------

The "Symfony Docker Default" project is compatible with symfony from 2.x to 5.x  
For quick usage, refer to [doc/get-started.md](doc/get-started.md)  

1. [Get started !](doc/get-started.md)    
2. [Configure Email provider](doc/configure-email-provider.md)  
3. [Configure Local database](doc/configure-local-database.md)  
4. [Configure remote database connection](doc/configure-remote-database-connection.md)  

Troubleshooting
----------------------------

The current examples uses docker-compose 3 version.  

```
